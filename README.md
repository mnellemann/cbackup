Cloud Backup
----------------------------------

Backup local files to any S3 compatible object store.

## Requirements

- Java 8 or later runtime.

## Usage

```shell
Usage: cbackup [-hVv] --access-key=<key> [--bucket=<bucket>] --endpoint=<url>
                --secret-key=<key> [--exclude=<path>]... --source=<path>
                [--source=<path>]...
      --access-key=<key>   Object store access-key.
      --bucket=<bucket>    Object store bucket [default: 'cbackup'].
      --endpoint=<url>     Object store endpoint.
      --exclude=<path>     Folder or file to exclude, can be repeated.
  -h, --help               Show this help message and exit.
      --secret-key=<key>   Object store secret-Key.
      --source=<path>      Folder or file to backup, can be repeated.
  -v, --verbose            Verbose output, can be repeated.
  -V, --version            Print version information and exit.
```

### Ignore Rules

If a **.nobackup** file is found, the directory, and everything below it, will be excluded/ignored.

if a **.gitignore** file is found, the simple rules excluding files or folder directly in the current directory, are used.


## Development


### Minio Server

Start a local minio in a container for testing:

```shell
docker run -p 9000:9000 \
  -e "MINIO_ACCESS_KEY=AKIAIOSFODNN7EXAMPLE" \
  -e "MINIO_SECRET_KEY=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY" \
  minio/minio server /tmp/cbackup
```

Perform a backup to the local minio:

```shell
java -jar build/libs/cbackup-x.y.z-all.jar \
  --source=/home/mark/Documents \
  --endpoint=http://localhost:9000 \
  --access-key=AKIAIOSFODNN7EXAMPLE \
  --secret-key=wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY \
  --bucket=cbackup --verbose
```
