/*
   Copyright 2020 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package biz.nellemann.cbackup;

import io.minio.*;
import io.minio.errors.*;
import io.minio.messages.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.ZonedDateTime;
import java.util.*;

public class MinioBackup {

    private final static Logger log = LoggerFactory.getLogger(MinioBackup.class);

    private final List<String> excludeFolderList;
    private final MinioClient minioClient;
    private final String s3Bucket;

    private final ObjectLockConfiguration objectLockConfiguration = new ObjectLockConfiguration(RetentionMode.COMPLIANCE, new RetentionDurationDays(365));
    private final Retention retentionPolicy = new Retention(RetentionMode.COMPLIANCE, ZonedDateTime.now().plusDays(365));


    /**
     * Event Listener Configuration
     */

    protected List<BackupEventListener> eventListeners = new ArrayList<>();
    public synchronized void addEventListener( BackupEventListener l ) {
        eventListeners.add( l );
    }
    public synchronized void removeEventListener( BackupEventListener l ) {
        eventListeners.remove( l );
    }


    MinioBackup(String endPoint, String accessKey, String secretKey, String s3Bucket, List<String> excludeFolderList) throws Exception {

        this.s3Bucket = s3Bucket;
        this.excludeFolderList = excludeFolderList;
        this.minioClient = MinioClient.builder()
            .endpoint(endPoint)
            .credentials(accessKey, secretKey)
            .build();

        // Check if the bucket already exists.
        if (!minioClient.bucketExists(BucketExistsArgs.builder().bucket(s3Bucket).build())) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(s3Bucket).build());
        }

    }


    public boolean upload(FileObject fileObject) {
        return upload(fileObject.getLocalPath(), fileObject.getRemoteObject());
    }

    public boolean upload(String localFile, String remoteObject) {

        log.info("upload() - " + localFile + " to " + remoteObject);

        try {
            UploadObjectArgs args = UploadObjectArgs.builder()
                .bucket(s3Bucket).object(remoteObject).filename(localFile).build();
            minioClient.uploadObject(args);
            sendEvent(new BackupEventMessage(BackupEventStatus.OK, localFile));
        } catch (MinioException | IOException | InvalidKeyException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


/*
    public void upload(String fileStr) {
        File f = new File(fileStr);
        try {
            upload(f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void upload(File localFile) {
        String remoteObject = localFile.toString().substring(1);
        FileObject fo = new FileObject(remoteObject, localFile);
        upload(localFile, remoteObject);
    }


    public boolean upload(FileObject fileObject, String remoteObject){

        // Check if remote object exists, and in that case, if checksums are equal (== no changes)
        String localFileChecksum;
        StatObjectResponse objectStat;
        try {
            localFileChecksum = FileUtil.getFileMd5Checksum(localFile);
            objectStat = remoteObjectStat(remoteObject);
        } catch (IOException | ErrorResponseException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }

        if(objectStat != null) {
            String remoteObjectChecksum = objectStat.etag();
            if(remoteObjectChecksum != null && remoteObjectChecksum.equalsIgnoreCase(localFileChecksum)) {
                log.debug("skipping file (same checksum): " + localFile + " to " + remoteObject);
                sendEvent(new BackupEventMessage(BackupEventStatus.SKIP_NO_CHANGE, localFile.toString()));
                // TODO: Is it possible to update remote-object, to avoid retention policies or similar to delete it ?
                return true;
            }
        }

        log.info("uploading: " + localFile + " to " + remoteObject);
        Map<String, String> userMetaData = new HashMap<>();
        userMetaData.put("LastModified", Long.toString(localFile.lastModified()));
        try {
            UploadObjectArgs args = UploadObjectArgs.builder()
                .bucket(s3Bucket).object(remoteObject).filename(localFile.toString()).userMetadata(userMetaData).build();
            minioClient.uploadObject(args);
            sendEvent(new BackupEventMessage(BackupEventStatus.OK, localFile.toString()));
        } catch (MinioException | IOException | InvalidKeyException | NoSuchAlgorithmException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
*/


    public boolean download(String remoteObject, String localFile) throws MinioException {
        DownloadObjectArgs args = DownloadObjectArgs.builder().bucket(s3Bucket).object(remoteObject).filename(localFile).build();
        StatObjectResponse objectStat = remoteObjectStat(remoteObject);
        if(objectStat != null) {
            log.info("download() - " + remoteObject + " to " + localFile);
            try {
                minioClient.downloadObject(args);
                return true;
            } catch (Exception e) {
                log.warn("downloadFile()", e);
            }
        }
        return false;
    }


    public boolean delete(String remoteObject) {

        DeleteObject obj = new DeleteObject(remoteObject);
        Set<DeleteObject> objectSet = new HashSet<>();
        objectSet.add(obj);

        return delete(objectSet);
    }


    public boolean delete(Set<DeleteObject> deleteObjectSet) {

        RemoveObjectsArgs args = RemoveObjectsArgs.builder().bucket(s3Bucket).objects(deleteObjectSet).build();
        try {
            minioClient.removeObjects(args);
            return true;
        } catch (Exception e) {
            log.warn("downloadFile()", e);
        }
        return false;
    }


    private StatObjectResponse remoteObjectStat(String remoteObject) throws ErrorResponseException {
        try {
            StatObjectResponse objectStat = minioClient.statObject(StatObjectArgs.builder().bucket(s3Bucket).object(remoteObject).build());
            return objectStat;
        } catch (ErrorResponseException e) {
            if(!e.errorResponse().code().equals("NoSuchKey")) {
                log.error(e.errorResponse().toString());
                throw e;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    private synchronized void sendEvent(BackupEventMessage eventMessage) {
        BackupEvent event = new BackupEvent( this, eventMessage );
        for (BackupEventListener eventListener : eventListeners) {
            eventListener.onBackupEvent(event);
        }
    }

}
