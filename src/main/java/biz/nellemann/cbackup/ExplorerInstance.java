package biz.nellemann.cbackup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ExplorerInstance implements Runnable {

    private final static Logger log = LoggerFactory.getLogger(ExplorerInstance.class);

    private final Database db;
    private final MinioBackup minioBackup;
    private final File localSourcePath;
    private final List<String> excludeFolderList;

    protected List<BackupEventListener> eventListeners = new ArrayList<>();
    public synchronized void addEventListener( BackupEventListener l ) {
        eventListeners.add( l );
    }
    public synchronized void removeEventListener( BackupEventListener l ) {
        eventListeners.remove( l );
    }


    ExplorerInstance(Database db, MinioBackup minioBackup, File localSourcePath, List<String> excludeFolderList) {
        this.db = db;
        this.minioBackup = minioBackup;
        this.localSourcePath = localSourcePath;
        this.excludeFolderList = excludeFolderList;
    }


    @Override
    public void run() {
        explore(localSourcePath);
    }


    protected void explore(File dir) {

        log.debug("explore() - " + dir.toString());

        if(!dir.exists() || !dir.isDirectory()) {
            log.debug("explore() - skipping, non-existing directory.");
            return;
        }

        // Check exclude rules
        if(excludeFolderList != null && excludeFolderList.size() > 0 && excludeFolderList.contains(dir.getAbsolutePath())) {
            log.debug("explore() - skipping, present in exclude rules.");
            return;
        }

        // Check for .nobackup file
        File noBackupFile = new File(dir, ".nobackup");
        if(noBackupFile.exists()) {
            log.debug("explore() - skipping, found .nobackup file.");
            //sendEvent(new BackupEventMessage(BackupEventStatus.SKIP_NO_BACKUP, localFolder.toString()));
            return;
        }

        // Check optional .gitignore file
        List<String> ignoreRules = new ArrayList<>();
        File ignoreFile = new File(dir, ".gitignore");
        if(ignoreFile.exists()) {

            List<String> lines;
            try {
                lines = Files.readAllLines(ignoreFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            for(String line : lines) {
                if(line.startsWith("*") || line.startsWith("!") || line.endsWith("*")) {
                    continue;
                }
                File tmpFile = new File(dir, line);
                if(tmpFile.exists()) {
                    ignoreRules.add(line);
                }
            }
        }

        File[] files = dir.listFiles();
        for(File f : Objects.requireNonNull(files)) {

            if(ignoreRules.size() > 0 && FileUtil.isIgnored(f, ignoreRules)) {
                log.debug("explore() - skipped, present in .gitignore.");
                //sendEvent(new BackupEventMessage(BackupEventStatus.SKIP_GITIGNORE, localFolder.toString()));
                continue;
            }

            if(f.isDirectory()) {
                explore(f);
            } else {
                log.info("explore() - processing: " + f);
                try {
                    process(f);
                } catch (UnsupportedEncodingException e) {
                    log.error(e.getMessage());
                }
            }
        }

    }


    private void process(File f) throws UnsupportedEncodingException {

        FileObject fo = db.addLocalFile(f);
        if(fo.isUpdated()) {
            minioBackup.upload(fo);
        }

    }


}
