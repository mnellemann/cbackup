/*
   Copyright 2020 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package biz.nellemann.cbackup;

import org.slf4j.impl.SimpleLogger;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@Command(name = "cbackup",
    mixinStandardHelpOptions = true,
    versionProvider = biz.nellemann.cbackup.VersionProvider.class)
public class Application implements Callable<Integer> {

    @Option(names = {"--source"}, required = true, paramLabel="<path>", description = "Folder or file to backup, can be repeated.")
    List<String> includePaths;

    @Option(names = {"--exclude"}, paramLabel="<path>", description = "Folder or file to exclude, can be repeated.")
    List<String> excludeRules;

    @Option(names = {"--endpoint"}, required = true, description = "Object store endpoint.", paramLabel = "<url>")
    String endPoint;

    @Option(names = {"--access-key"}, required = true, description = "Object store access-key.", paramLabel = "<key>")
    String accessKey;

    @Option(names = {"--secret-key"}, required = true, description = "Object store secret-Key.", paramLabel = "<key>")
    String secretKey;

    @Option(names = {"--bucket"}, required = true, defaultValue = "cbackup", description = "Object store bucket [default: 'cbackup'].")
    String bucket = "s3backup";

    @CommandLine.Option(names = { "-v", "--verbose" }, description = "Verbose output, can be repeated.")
    private boolean[] verbose = new boolean[0];


    public static void main(String... args) {
        int exitCode = new CommandLine(new Application()).execute(args);
        System.exit(exitCode);
    }


    @Override
    public Integer call() {

        switch (verbose.length) {
            case 1:
                System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "INFO");
                break;
            case 2:
                System.setProperty(SimpleLogger.DEFAULT_LOG_LEVEL_KEY, "DEBUG");
                break;
        }

        // Try to connect to remote
        MinioBackup minioBackup;
        try {
            minioBackup = new MinioBackup(endPoint, accessKey, secretKey, bucket, excludeRules);
        } catch(Exception e) {
            System.err.println(e.getMessage());
            return -1;
        }

        // Download remote database, or create a new empty file locally
        File dataFile;
        try {
            dataFile = File.createTempFile("cbackup-", ".db");
            dataFile.deleteOnExit();
            if(!minioBackup.download("cbackup.db", dataFile.getAbsolutePath())) {
                System.err.println("No remote database found, creating new empty file.");
                dataFile.createNewFile();
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return -1;
        }

        // Open local database
        Database db = new Database(dataFile);
        db.load();

        // Scan the local sources and process
        List<Thread> threadList = new ArrayList<>();
        for(String str : includePaths) {
            File includePath = new File(str);
            if(includePath.exists() && includePath.isDirectory()) {
                try {
                    Thread t = new Thread(new ExplorerInstance(db, minioBackup, includePath, excludeRules));
                    t.start();
                    threadList.add(t);
                } catch(Exception e) {
                    System.err.println(e.getMessage());
                }
            }
        }

        // Wait for threads to finish
        for (Thread thread : threadList) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // TODO: Process database and delete remote files marked for deletion

        // Upload local copy of database to remote location
        try {
            db.save();
            minioBackup.upload(dataFile.getAbsolutePath(), "cbackup.db");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }

}
