/*
   Copyright 2020 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package biz.nellemann.cbackup;

public enum BackupEventStatus {

    OK(1, "Upload OK"),
    SKIP_NO_CHANGE(5, "File skipped"),
    SKIP_NO_BACKUP(7, "Folder skipped"),
    SKIP_GITIGNORE(8, "Folder skipped"),
    UNKNOWN(99, "Unknown event");

    private final int id;
    private final String message;

    BackupEventStatus(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() { return id; }
    public String getMessage() { return message; }

}
