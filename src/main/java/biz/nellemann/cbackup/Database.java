package biz.nellemann.cbackup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

public class Database {

    private final static Logger log = LoggerFactory.getLogger(Database.class);

    private final File file;
    private Map<String, FileObject> fileObjects = new HashMap<>();


    Database(File file) {
        this.file = file;
    }



    protected void load() {

        if(file.length() < 1) {
            return;
        }

        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            Object thisObject = ois.readObject();
            if (thisObject instanceof HashMap) {
                fileObjects = (Map<String, FileObject>) thisObject;
            }

            ois.close();
            fis.close();
        } catch(IOException ioe) {
            ioe.printStackTrace();
            return;
        } catch(ClassNotFoundException c) {
            log.warn("load() - Class not found");
            c.printStackTrace();
            return;
        }

        log.debug("load() - Deserialized HashMap loaded from: " + file);

    }


    protected void save() {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(fileObjects);
            oos.close();
            fos.close();
            log.debug("save() - Serialized HashMap data is saved into: " + file);
        } catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }


    protected FileObject addLocalFile(File f) throws UnsupportedEncodingException {
        FileObject fo;
        String key = f.getAbsolutePath();
        if(fileObjects.containsKey(key)) {
            fo = fileObjects.get(key);
            fo.refresh();
        } else {
            log.debug("addLocalFile() - " + f);
            fo = new FileObject(f);
            fileObjects.put(key, fo);
        }
        return fo;
    }


    protected Set<FileObject> getUpdatedFiles() {

        log.debug("getUpdatedFiles()");
        Set<FileObject> fileObjectSet = new HashSet<FileObject>();

        if(fileObjects == null || fileObjects.size() < 1) {
            return fileObjectSet;
        }

        for (Map.Entry<String, FileObject> stringFileObjectEntry : fileObjects.entrySet()) {
            FileObject fileObject = (FileObject) ((Map.Entry) stringFileObjectEntry).getValue();
            if (fileObject.isUpdated()) {
                fileObjectSet.add(fileObject);
            }
        }
        return fileObjectSet;
    }

}
