/*
   Copyright 2020 mark.nellemann@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package biz.nellemann.cbackup;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class FileUtil {

    public static String getFileMd5Checksum(File file) {

        String md5sum;
        MessageDigest messageDigest;

        try {
           messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

        try(FileInputStream fis = new FileInputStream(file);
            DigestInputStream dis = new DigestInputStream(fis, messageDigest);
            InputStreamReader isr = new InputStreamReader(dis);
            BufferedReader br = new BufferedReader(isr)) {
            String data = br.readLine();
            while (data != null) {
                data = br.readLine();
            }
            br.close();
            byte[] digest = dis.getMessageDigest().digest();
            md5sum = String.format("%032X", new BigInteger(1, digest));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return md5sum;
    }


    public static boolean isIgnored(File localFile, List<String> ignoreList) {

        String localFilePath = localFile.getAbsolutePath();

        for(String str : ignoreList) {
            String tmp = localFilePath.substring(localFilePath.lastIndexOf('/')+1);

            if(localFile.isDirectory() && str.endsWith("/")) {
                if(tmp.equals(str.substring(0, str.length() - 1))) {
                    return true;
                }
            }

            if(localFile.isFile() && !str.endsWith("/")) {
                if(tmp.equals(str)) {
                    return true;
                }
            }

        }

        return false;
    }


}
