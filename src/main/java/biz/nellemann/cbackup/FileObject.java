package biz.nellemann.cbackup;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.Serializable;

public class FileObject implements Serializable {

    private final static Logger log = LoggerFactory.getLogger(FileObject.class);

    private static final long serialVersionUID = 1L;

    private transient boolean updated;

    private final String remoteObject;
    private final String localPath;
    private String localChecksum;


    FileObject(File f) {
        this.localPath = f.getAbsolutePath();
        this.localChecksum = FileUtil.getFileMd5Checksum(f);

        // What about Windows files? Remove c:// ?
        String tmp;
        if(File.separatorChar == '/') {
            tmp = localPath.substring(1);
        } else {
            tmp = localPath.substring(2);
        }
        //this.remoteObject = URLEncoder.encode(tmp, "UTF-8");
        this.remoteObject = tmp;
        this.updated = true;
    }


    public void refresh() {
        String tmpChecksum = FileUtil.getFileMd5Checksum(new File(localPath));
        if(tmpChecksum != null && tmpChecksum.equals(localChecksum)) {
            log.debug("refresh() - same checksum, file not changed: " + localPath);
            return;
        }
        updated = true;
        this.localChecksum = tmpChecksum;
    }


    public boolean isUpdated() {
        return updated;
    }

    public String getLocalPath() {
        return localPath;
    }

    public String getRemoteObject() {
        return remoteObject;
    }

}
