package biz.nellemann.cbackup

import spock.lang.Specification

class FileUtilTest extends Specification {

    File testFile

    void setup() {
        testFile = new File(getClass().getResource('/test.txt').toURI())
    }

    // Test getFileMd5Checksum
    void "test getFileMd5Checksum() with tested file"() {
        when:
        def md5sum = FileUtil.getFileMd5Checksum(testFile)

        then:
        md5sum == "D41D8CD98F00B204E9800998ECF8427E"
    }

    void "test isIgnored() with tested file being ignored"() {
        setup:
        def ignoreList = [ 'foo/bar.txt', 'test.txt']

        when:
        def isIgnored = FileUtil.isIgnored(testFile, ignoreList)

        then:
        isIgnored
    }

    void "test isIgnored() with tested file not being ignored"() {
        setup:
        def ignoreList = [ 'foo/bar.txt', 'logger.xml']

        when:
        def isIgnored = FileUtil.isIgnored(testFile, ignoreList)

        then:
        !isIgnored
    }

}


